﻿Trabajo realizado

El objetivo es encontrar el alojamiento adecuado según las preferencias del usuario

Datos 

1. Cargamos los datos de airbnb
2. Además, se carga otra fuente de datos que contiene información de las ciudades, como su localización (centro de la ciudad). También contiene la temperatura media mensual desde 1960-1990 de las ciudades. Aunque solo se usará la media anual
3. Filtramos eliminando los campos del host size nulos y además nos quedamos con Europa.
4. Creamos un parámetro calculado, que es el precio por persona del alojamiento, el KPI que será de base del Dashboard
5. Creamos otro parámetro calculado que es la distancia del centro de la ciudad al alojamiento, también KPI importante en el Dashboard

Gráfico de Mapa Europa 

6. creamos gráfico del mapa de Europa.
7. Generamos filtro para elegir el tipo de alojamiento, el número de personas para el alojamiento, y el intervalo de precio por persona que estamos dispuestos a pagar.
8. Coloreamos el mapa en función del precio por persona
9. creamos un Pie chart con un grupo para la política de cancelación en función de si es estricta, moderada o flexible.
10.  En la leyenda además le añadimos la temperatura media del país

Ranking

11. Creamos un gráfico que muestre la diferencia de precio de ese país con respecto a la media, lo hacemos mediante fixed. El campo calculado se Precio medio total.

Temperatura media

12. Gráfico de barras que muestra la temperatura media anual de 1960-1990 en las diferentes ciudades. Por problemas con los nombres de las ciudades se hacen grupos para agrupar aquellas que se hayan escrito de diferente forma.

Tabla top

13. Se realiza una tabla con los filtros de la ciudad, la distancia máxima al centro y la nota mínima de los clientes.
14. Se crea un campo calculado que se llama top donde pondera la nota según distancia y la nota según review. El campo Nota según distancia se ha tenido que calcular también

15. Generamos una tabla que muestre de forma ordenada según el top creado, y además muestre la información más relevante del alojamiento, así como la url para poder buscarlo en la web y reservar.

Cuadro distancia vs Precio

16. Por último se crea un gráfico con los puntos mostrados en la tabla, de tal forma que podamos ver el binomio precio y distancia y ayude a nuestra elección.


